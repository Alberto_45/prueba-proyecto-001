package pkgColegio;

public class Alumno {

    //Atributos
    private final Colegio nombreColegio;
    private final String nombreAlumno;
    private double notaMedia;
    private final int nAlumno;
    private static int numeroAlumno=1;

    //Metodos
    public Alumno(Colegio nombreColegio,String nombreAlumno, double notaMedia) {
        this.nombreAlumno = nombreAlumno;
        this.notaMedia = notaMedia;
        this.nombreColegio = nombreColegio;
        nAlumno=numeroAlumno;
        numeroAlumno++;
    }
    public void setNotaMedia(double notaMedia) {

        this.notaMedia = notaMedia;
    }

    public String toString () {

        return "Nombre alumno: " + nombreAlumno + "\n" +
                "Colegio: " + this.getNombreColegioAlumno() + "\n" +
                "N.Alumno: "+nAlumno+"\n" +
                "Nota media: "+notaMedia;
    }
    public String getNombreColegioAlumno(){

        return this.nombreColegio.getNombreColegio();

    }
    public String getNombreAlumno(){

        return nombreAlumno;
    }




}
